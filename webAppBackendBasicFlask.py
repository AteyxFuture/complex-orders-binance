"""Very basic Flask server taken from another project;)"""
from flask import Flask, url_for
app = Flask(__name__)

@app.route("/") # take note of this decorator syntax, it's a common pattern
def home():
	return insertAPIkeys() # Form to insert access keys by default.

@app.route("/displayPoint")
def insertAPIkeys():
	""" Display form for keys into user's Binance account: """
	return r"""<!DOCTYPE html> <html>	  <head>	  </head>	  <body> <h3>Enter Binance account keys: </h3>	..     </body>	</html>"""


@app.route("/key,secret/<int:keyNum>,<int:secretNum>") # test column:
def dummySomthingForTheKey(keyNum, secretNum):
	return "Keys are "+str((keyNum, secretNum))

@app.route("/api") # http://flask.pocoo.org/snippets/117 would offer a more informative aproach to the API
def api_listing():
	listing = ""
	with app.test_request_context():
		listing += url_for('home')
		listing += url_for('displayPath')
	return listing


if __name__ == "__main__":
	# app.run() # Production with uWSGI and NGINGX
	app.run(host='0.0.0.0', port=8080, debug=True)
	# ^@toDo: enclose that with `if sys.flags.debug`: and run as `python3 -d ` in the case of docker run .. image:tag -d 